<?php

// this file is used for importing the data the SQL database

include 'autoload.php';

$row = 0;
$fpath = './american-election-tweets_1.csv';

$f = [
    "handle" => 0,
    "text" => 1,
    "is_retweet" => 2,
    "original_author" => 3,
    "time" => 4,
    "in_reply_to_screen_name" => 5,
    "is_quote_status" => 6,
    "retweet_count" => 7,
    "favorite_count" => 8,
    "source_url" => 9,
    "truncated" => 10
];

// list containing all db entries (array lookup is cheaper than db lookup)
$handleList = [];
$hashtagList = [];
$tweetList = [];

$hashtagQuery = "SELECT * FROM `Hashtag`";
$handleQuery = "SELECT * FROM `Account`";
$tweetQuery = "SELECT `time`, `handle` FROM `Tweet`";

/*
 * get hashtags from database
 */
$hashtagResult = $conn->query($hashtagQuery);

if ($hashtagResult->num_rows > 0) {

    while($row = $hashtagResult->fetch_assoc()) {

        $hashtagList[] = $row['name'];

    }

} else {

    echo "0 hashtags found\n";

}

$hashtagResult->close();

/*
 * get handles/accounts from database
 */
$handleResult = $conn->query($handleQuery);

if ($handleResult->num_rows > 0) {

    while($row = $handleResult->fetch_assoc()) {

        $handleList[] = $row['handle'];

    }

} else {

    echo "0 accounts found\n";

}

$handleResult->close();

/*
 * get tweets from database
 */
$tweetResult = $conn->query($tweetQuery);

if ($tweetResult->num_rows > 0) {

    while($row = $tweetResult->fetch_assoc()) {

        $tweetList[] = md5($row['time'] . $row['handle']);

    }

} else {

    echo "0 tweets found\n";

}

$tweetResult->close();

function startsWith($needle, $haystack)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

if (($fhandle = fopen($fpath, "r")) !== false) {

    // iterating over all entries
    while (($data = fgetcsv($fhandle, 1000, ";")) !== false) {

        $row++;

        if ($row == 1) continue;

        /*
         * import the author
         *
         *  1. lookup the handle in array $handleList
         *  2. if not found: insert into table Election.Account
         *  3. if success: add handle to array $handleList
         *
         */

        $handle = $data[$f['handle']];

        if (!in_array($handle, $handleList)) {

            // insert into database

            $sql = "INSERT INTO `Account` (`handle`) VALUES (?)";

            $stmt = $conn->prepare($sql);
            $stmt->bind_param("s", $handle);

            if ($stmt->execute() == TRUE) {
                echo "New record created successfully\n";
            } else {
                echo "Error: " . $sql . "\n" . $stmt->error;
            }

            $stmt->close();

            $handleList[] = $handle;

        }

        /*
         * import tweet
         *
         *  1. ('', handle, time, text, retweet_count, favorite_count, original_author, is_quote_status)
         *  2. insert into table Election.Tweet
         *  3. get insertid and save into variable
         *
         */

        $time = $data[$f['time']];

        $time = date('Y-m-d H:i:s', strtotime($time)); // format to mysql datetime format
        $tweetKey = md5($time . $handle); // minimal key to identify tweet (hashed to make handling easier)

        $tweetInsertID = 0;

        if (!in_array($tweetKey, $tweetList)) {

            $text = $data[$f['text']];
            $rCount = $data[$f['retweet_count']];
            $fCount = $data[$f['favorite_count']];
            $oAuthor = $data[$f['original_author']];
            $isQuote = $data[$f['is_quote_status']];

            $isQuote = ($isQuote == 'False') ? 0 : 1;

            /*
             *  insert tweet in database
             *  return insert id
             */

            // insert into database

            $sql = "INSERT INTO `Tweet` (`handle`, `time`, `text`, `retweet_count`, `favorite_count`, `original_author`, `is_quote_status`) VALUES (?, ?, ?, ?, ?, ?, ?)";

            $stmt = $conn->prepare($sql);
            $stmt->bind_param("sssssss", $handle, $time, $text, $rCount, $fCount, $oAuthor, $isQuote);

            if ($stmt->execute() == TRUE) {
                echo "New tweet created successfully\n";
            } else {
                echo "Error: " . $sql . "\n" . $stmt->error . "\n";
            }

            $tweetInsertID = $conn->insert_id;

            $stmt->close();

            $tweetList[] = $tweetKey;

        }

        /*
         * import hashtags
         *
         *  1. search for hashtags in text
         *  2. strip string from "#"
         *  3. lookup hashtag in array $hashtagList
         *  4. if not found: insert into table Election.Hashtag
         *  5. if success: add hashtag to array $hashtagList
         *
         */

        $text = $data[$f['text']];

        $textArray = explode(" ", $text); // --> doesn't always work, because not only " " are used to delimit words / links etc.
        $textArray = preg_split("/[^\w]*([\s]|$)/", $text, -1, PREG_SPLIT_NO_EMPTY);

        foreach ($textArray as $word) {

            if (startsWith('#', $word)) {

                // isHashtag

                $word = htmlentities($word, ENT_COMPAT, "utf-8");

                // hashtag contains multibyte characters and is therefore not valid
                if (empty($word)) continue;

                $hashtag = substr($word, 1); // remove "#" from word -> plain hashtag name

                if (!in_array($hashtag, $hashtagList)) {

                    /*
                     *  insert into database
                     */

                    // insert into database

                    $sql = "INSERT INTO `Hashtag` (`name`) VALUES (?)";

                    $stmt = $conn->prepare($sql);
                    $stmt->bind_param("s", $hashtag);

                    if ($stmt->execute() == TRUE) {
                        echo "New hashtag created successfully\n";
                    } else {
                        echo "Error: " . $sql . "\n" . $stmt->error . "\n";
                    }

                    $stmt->close();

                    $hashtagList[] = $hashtag;

                }

                /*
                 * import hasHashtag
                 *
                 *  1. insert into table Election.hasHashtag (hashtag, tweetID)
                 *
                 */

                // insert into database

                $sql = "INSERT INTO `hasHashtag` (`hashtag`, `tweet`) VALUES (?, ?)";

                $stmt = $conn->prepare($sql);
                $stmt->bind_param("ss", $hashtag, $tweetInsertID);

                if ($stmt->execute() == TRUE) {
                    echo "New hasHashtag created successfully\n";
                } else {
                    echo "Error: " . $sql . "\n" . $stmt->error . "\n";
                }

                $stmt->close();

            }

            /*
             * import mentions
             *
             *  1. search for mentions in text
             *  2. strip string from "@"
             *  3. insert into table Election.mentions (tweetID, handle)
             *
             */
            if (startsWith('@', $word)) {

                // isMention

                $word = htmlentities($word, ENT_COMPAT, "utf-8");

                // hashtag contains multibyte characters and is therefore not valid
                if (empty($word)) continue;

                $mention = substr($word, 1, 16); // remove "@" from word -> plain handle
                $mention = preg_replace('/[^A-Za-z0-9\-]/', '', $mention); // remove special chars

                if (strlen($mention) > 15) continue; // ignore handles longer than allowed

                // insert into database

                $sql = "INSERT INTO `mentions` (`account`, `tweet`) VALUES (?, ?)";

                $stmt = $conn->prepare($sql);
                $stmt->bind_param("ss", $mention, $tweetInsertID);

                if ($stmt->execute() == TRUE) {
                    echo "New mention created successfully\n";
                } else {
                    echo "Error: " . $sql . "\n" . $stmt->error . "\n";
                }

                $stmt->close();

            }

        }


        /*
         * import repliesTo
         *
         *  1. if in_reply_to_screename is not empty:
         *  2. insert into table Election.repliesTo (tweetID, handle)
         *
         */

        $replyHandle = $data[$f['in_reply_to_screen_name']];

        if (!empty($replyHandle)) {

            // is reply

            $replyHandle = htmlentities($replyHandle, ENT_COMPAT, "utf-8");

            // hashtag contains multibyte characters and is therefore not valid
            if (empty($replyHandle)) continue;

            // insert into database

            $sql = "INSERT INTO `repliesTo` (`account`, `tweet`) VALUES (?, ?)";

            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ss", $replyHandle, $tweetInsertID);

            if ($stmt->execute() == TRUE) {
                echo "New reply created successfully\n";
            } else {
                echo "Error: " . $sql . "\n" . $stmt->error . "\n";
            }

            $stmt->close();

        }

    }

    fclose($fhandle); // close file stream

    $conn->close();

}
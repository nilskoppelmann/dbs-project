<?php

include 'autoload.php';

$metricQuery = "SELECT hh1.hashtag as h1, hh2.hashtag as h2, COUNT(*) as count
                FROM hasHashtag AS hh1 JOIN
                     hasHashtag AS hh2
                     ON hh1.tweet = hh2.tweet
                WHERE hh1.hashtag != hh2.hashtag
                GROUP BY hh1.hashtag, hh2.hashtag;";

$metricResult = $conn->query($metricQuery);

$nodes = [];
$edges = [];
$hashes = [];
$nodesOut = [];

if ($metricResult->num_rows > 0) {

    while ($row = $metricResult->fetch_assoc()) {

        $they = [$row['h1'], $row['h2']];
        sort($they);

        $hash = md5($they[0] . $they[1]);

        if (!in_array($hash, $hashes)) {

            if (!in_array($they[0], $nodes)) {
                $nodesOut[] = ["name" => $they[0], "group" => 1];
                $nodes[] = $they[0];
            }
            if (!in_array($they[1], $nodes)) {
                $nodesOut[] = ["name" => $they[1], "group" => 1];
                $nodes[] = $they[1];
            }

            $s = array_search($they[0], $nodes);
            $t = array_search($they[1], $nodes);

            $edges[] = [
                "source" => $s,
                "target" => $t,
                "weight" => $row["count"]
            ];

            $hashes[] = $hash;
        }

    }
}

$json = json_encode(["nodes" => $nodesOut, "links" => $edges]);

//var_dump($json);
file_put_contents('graphFile.json', $json);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>DBS Projekt Netzwerk Visualisierung</title>
</head>
<body>

<script src="http://d3js.org/d3.v2.min.js?2.9.3"></script>
<style>

    .link {
        stroke: #aaa;

    }

    .node text {
        stroke: #333;
        cursos: pointer;
        font-family: 'Helvetica Neue';
        font-weight: 400;
        font-size: 10px;
    }

    .node circle {
        stroke: #fff;
        stroke-width: 3px;
        fill: #555;
    }

</style>
<script>

    var width = 1920,
        height = 1200;

    var svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height);

    var force = d3.layout.force()
        .gravity(.1)
        .distance(75)
        .charge(-100)
        .size([width, height]);

    d3.json("graphFile.json", function (json) {
        force
            .nodes(json.nodes)
            .links(json.links)
            .start();

        var link = svg.selectAll(".link")
            .data(json.links)
            .enter().append("line")
            .attr("class", "link")
            .style("stroke-width", function (d) {
                return Math.sqrt(d.weight);
            });

        var node = svg.selectAll(".node")
            .data(json.nodes)
            .enter().append("g")
            .attr("class", "node")
            .call(force.drag);

        node.append("circle")
            .attr("r", "5");

        node.append("text")
            .attr("dx", 12)
            .attr("dy", ".35em")
            .text(function (d) {
                return d.name
            });

        force.on("tick", function () {
            link.attr("x1", function (d) {
                return d.source.x;
            })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                });

            node.attr("transform", function (d) {
                return "translate(" + d.x + "," + d.y + ")";
            });
        });
    });

</script>

<script>
</script>
</body>
</html>

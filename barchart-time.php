<?php

include 'autoload.php';


$metricQueryStatic = "SELECT COUNT(hashtag) as count, DATE(time) as time FROM Tweet t
                JOIN hasHashtag h
                ON h.tweet = t.id
                GROUP BY DATE(time);";

$metricResultStatic = $conn->query($metricQueryStatic);

$datesStatic = [];
$countStatic = [];

if ($metricResultStatic->num_rows > 0) {

    while ($row = $metricResultStatic->fetch_assoc()) {

        $date = strftime('%d.%m.%Y', strtotime($row['time']));

        $datesStatic[] = $date;
        $countStatic[] = $row['count'];

    }

}

$hashtagQuery = "SELECT name FROM Hashtag;";
$hashtagResult = $conn->query($hashtagQuery);
$hashtagsStandalone = [];

if ($hashtagResult->num_rows > 0) {

    while ($row = $hashtagResult->fetch_assoc()) {
        $hashtagsStandalone[] = $row['name'];
    }

}

$get = $_GET['hashtag'];

if (!isset($_GET['hashtag']) or empty($get)) {

    $panelInteractive = "<div class=\"panel panel-info\">Tragen Sie einen Hashtag in das Formular ein und klicken Sie auf \"analysieren\".</div>";

}

$metricQueryInteractive = 'SELECT COUNT(hashtag) as count, DATE(time) as time FROM Tweet t
                JOIN hasHashtag h
                ON h.tweet = t.id
                WHERE hashtag = "'.$get.'"
                GROUP BY DATE(time);';

$metricResultInteractive = $conn->query($metricQueryInteractive);

$datesInteractive = [];
$countInteractive = [];

if ($metricResultInteractive->num_rows > 0) {

    while ($row = $metricResultInteractive->fetch_assoc()) {

        $date = strftime('%d.%m.%Y', strtotime($row['time']));

        $datesInteractive[] = $date;
        $countInteractive[] = $row['count'];

    }

}

?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>DBS Projekt Visualisierungen</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">


</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <canvas id="barchartStatic"></canvas>
        </div>
        <div class="col-md-6">

            <form class="form" action="/barchart-time.php" method="get">
                <div class="input-group">
                    <select name="hashtag" id="hashtag" class="form-control"></select>
                    <span class="input-group-btn"><input type="submit" value="analysieren" class="btn btn-primary"></span>
                </div>
            </form>

            <canvas id="barchartInteractive"></canvas>
        </div>

    </div>

</div>
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">

    /*
     Populate Hashtags
     */

    var selectOptions = <?= json_encode($hashtagsStandalone) ?>;

    $.each(selectOptions, function (i, value) {
        $('#hashtag').append($('<option>').text(value).attr('name', value));
    });

    /*
     Static
     */
    var datesStatic = <?= json_encode($datesStatic) ?>;
    var countStatic = <?= json_encode($countStatic) ?>;

    var optionsStatic = {
        responsive: true,
        title: {
            display: true,
            text: "Das Aufkommen von Hashtags auf Zeit"
        }
    };

    var dataStatic = {
        labels: datesStatic,
        datasets: [{
            label: "Wie oft treten die Hashtags auf?",
            backgroundColor: '#ff8800',
            borderColor: '#ffed00',
            data: countStatic,
        }]
    };

    var ctxStatic = document.getElementById('barchartStatic').getContext('2d');
    var chartStatic = new Chart(ctxStatic, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: dataStatic,
        options: optionsStatic
    });

    /*
     Interactive
     */

    var datesInteractive = <?= json_encode($datesInteractive) ?>;
    var countInteractive = <?= json_encode($countInteractive) ?>;

    var optionsInteractive = {
        responsive: true,
        title: {
            display: true,
            text: "Interactive Hashtag Visualization"
        }
    };

    var dataInteractive = {
        labels: datesInteractive,
        datasets: [{
            label: "Wie oft tritt der \"<?= $get ?>\" als Hashtag auf?",
            backgroundColor: '#ff8800',
            borderColor: '#ffed00',
            data: countInteractive,
        }]
    };

    var ctxInteractive = document.getElementById('barchartInteractive').getContext('2d');
    var chartInteractive = new Chart(ctxInteractive, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: dataInteractive,
        options: optionsInteractive
    });
</script>

</body>

</html>
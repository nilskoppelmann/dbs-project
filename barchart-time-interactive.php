<?php

include 'autoload.php';

$metricQuery = "SELECT COUNT(hashtag) as count, DATE(time) as time FROM Tweet t
                JOIN hasHashtag h
                ON h.tweet = t.id
                WHERE 
                GROUP BY DATE(time);";

$metricResult = $conn->query($metricQuery);

$dates = [];
$count = [];

if ($metricResult->num_rows > 0) {

    while ($row = $metricResult->fetch_assoc()) {

        $date = strftime('%d.%m.%Y', strtotime($row['time']));

        $dates[] = $date;
        $count[] = $row['count'];

    }

}

?>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>DBS Projekt Barchart Visualisierung</title>
</head>
<body>
<canvas id="barchart"></canvas>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript">

    var datesInteractive = <?= json_encode($dates) ?>;
    var countInteractive = <?= json_encode($count) ?>;

    var optionsInteractive = {
        responsive: true,
        title: {
            display: true,
            text: "Interactive Hashtag Visualization"
        }
    };

    var dataInteractive = {
        labels: datesInteractive,
        datasets: [{
            label: "Wie oft treten die Hashtags auf?",
            backgroundColor: '#ff8800',
            borderColor: '#ffed00',
            data: countInteractive,
        }]
    };

    var ctxInteractive = document.getElementById('barchart').getContext('2d');
    var chartInteractive = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: dataInteractive,
        options: optionsInteractive
    });
</script>

</body>

</html>
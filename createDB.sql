SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `Election`;
CREATE DATABASE `Election` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `Election`;

DROP TABLE IF EXISTS `Account`;
CREATE TABLE `Account` (
  `handle` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `hasHashtag`;
CREATE TABLE `hasHashtag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hashtag` varchar(139) COLLATE utf8_bin NOT NULL,
  `tweet` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `Hashtag`;
CREATE TABLE `Hashtag` (
  `name` varchar(139) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `mentions`;
CREATE TABLE `mentions` (
  `tweet` int(11) NOT NULL,
  `account` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `repliesTo`;
CREATE TABLE `repliesTo` (
  `account` varchar(15) COLLATE utf8_bin NOT NULL,
  `tweet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `Tweet`;
CREATE TABLE `Tweet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(15) COLLATE utf8_bin NOT NULL,
  `time` datetime NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `retweet_count` int(11) NOT NULL,
  `favorite_count` int(11) NOT NULL,
  `original_author` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `is_quote_status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

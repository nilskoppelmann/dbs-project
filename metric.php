<?php

include 'autoload.php';

$together = []; // md5(h1 + h2) = [[h1, h2], count, distance]

$distance = []; // contains all elements with distance as key (important for k-means)

$maxDist = 0;

$metricQuery = "SELECT hh1.hashtag as h1, hh2.hashtag as h2, COUNT(*) as count
                FROM hasHashtag AS hh1 JOIN
                     hasHashtag AS hh2
                     ON hh1.tweet = hh2.tweet
                WHERE hh1.hashtag != hh2.hashtag
                GROUP BY hh1.hashtag, hh2.hashtag;";

$metricResult = $conn->query($metricQuery);

if ($metricResult->num_rows > 0) {

    while($row = $metricResult->fetch_assoc()) {

        $they = [$row['h1'], $row['h2']];

        sort($they);

        $hash = md5($they[0] . $they[1]);

        if (in_array($hash, $together)) continue; // ignore duplicates

        $together[$hash] = [$they, (int) $row['count']];

        // update max (+1 for minimal distance)
        if ($row['count'] > $maxDist) $maxDist = $row['count'] + 1;

    }

}

/*
 * calculate distance (reversed proportionality)
 */

foreach ($together as $k => $one) {

    $dist = $maxDist - $one[1];

    $together[$k] = [$one[0], $one[1], $dist];

}

/*
 *
 *
 * K-MEANS
 *
 *
 */

$k = 5;
$t = 0;

$data = $together;
$centroids = array_rand($data, $k);
$means = [];


function groupReorder() {

    global $data, $centroids;

    $rest = array_diff($data, $centroids);

    $newData = [];

    foreach ($rest as $k => $item) {

        // $item[2] contains the distance

        $diffs = [];

        foreach ($this->centroids as $c) {

            $centroid = $data[$c];

            var_dump($centroid);

            $diffs[$c] = abs($item[2] - $centroid[2]);

        }

        var_dump($diffs);

    }

}

groupReorder();
